/*@codekit-append "../sections/form/form.js"*/

var site;

function Site () {

	var instance, jQueries, internal;
	instance = this;
	internal = {};
	jQueries = {};

	function onReady ( _event ) {
	  window.isReady = true;
	  createFormSection();
	  jQuery('.header-menu-button').click(toggleMenu);
	  createCopyrightDate();
	}
	
	function createFormSection ( _index, _element ) {
	  var _module;
	  _module = new FormSection();
	  _module.setElementJQuery ( _element );
	  _module.init();
  }
  
  function createCopyrightDate() {
    var date = new Date();
    var year = date.getFullYear();
    document.getElementById("copyright-text").innerHTML = 'Copyright ' + year +  '. All Rights Reserved.';
  }
  
  function toggleMenu(){
    if (jQuery('.overflow-wrap').hasClass('menu-is-open')){
      jQuery('.overflow-wrap').removeClass('menu-is-open');
      jQuery('.overflow-wrap').addClass('menu-is-closed');
    }
    else {
      jQuery('.overflow-wrap').addClass('menu-is-open');
      jQuery('.overflow-wrap').removeClass('menu-is-closed');
    }
  }
    
    /*
$('.grid').masonry({
      itemSelector: '.grid-item',
      columnWidth: '.grid-sizer',
      percentPosition: true
    });
*/
  equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.grid .squared');
});


$(window).resize(function(){
  equalheight('.grid .squared');
});



    
	jQuery( onReady );
	

}

site = new Site();